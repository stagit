#include <stdio.h>
#include <stddef.h>

int
cp(const char fileSource[], const char fileDestination[])
{
	int c;
	FILE *stream_R, *stream_W; 

	stream_R = fopen(fileSource, "r");
	if (stream_R == NULL)
		return -1;
	stream_W = fopen(fileDestination, "w");   //create and write to file
	if (stream_W == NULL)
	{
		fclose(stream_R);
		return -2;
	}    
	while ((c = fgetc(stream_R)) != EOF)
		fputc(c, stream_W);
	fclose(stream_R);
	fclose(stream_W);

	return 0;
}
