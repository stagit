#!/bin/sh

fresh=false
unique=false

for arg in "$@"; do
	[ "$unique" = true ]   && unique="$arg"
	[ "$arg" = "--only" ]  && unique=true
	[ "$arg" = "--fresh" ] && fresh=true
done

[ "$unique" = true ] && {
	echo "Expected argument after \`--only\`.";
	exit 1;
}

STAGIT=/var/www/git/stagit.out
[ ! -f "$STAGIT" ] && STAGIT=stagit

STAGIT_INDEX=/var/www/git/stagit-index.out
[ ! -f "$STAGIT_INDEX" ] && STAGIT_INDEX=stagit-index

build_html () {
	repo="$1"
	repo="$(basename "$repo" | sed 's/\.git$//g')"

	[ "$fresh" = true ] && {
		echo "Deleting HTML for $repo.";
		rm -fr "/var/www/git/$repo";
	}
	mkdir -p "/var/www/git/$repo"
	cd "/var/www/git/$repo" || { echo "Couldn't cd."; exit 1; }

	[ ! -f style.css ]   && ln -s ../style.css ./
	[ ! -f favicon.png ] && ln -s ../favicon.png ./
	[ ! -f logo.png ]    && ln -s ../logo.png ./
	[ ! -f highlight ]   && ln -s ../highlight ./
	[ ! -f index.html ]  && ln -s log.html index.html

	echo "git://git.knutsen.co/$repo" > "/srv/git/$repo.git/url"

	COMMAND="$STAGIT -c ./cachefile /srv/git/$repo.git"
	echo "Building web-page for $repo."
	echo "$COMMAND"
	$COMMAND
}

if [ "$unique" = false ]; then
	for repo in /srv/git/*.git; do
		build_html "$repo"
	done
else
	build_html "$unique"
fi

echo "Generating index.html with \`$STAGIT_INDEX\`."
"$STAGIT_INDEX" /srv/git/*.git > /var/www/git/index.html

# Correct ownership of the web files.
chown git:www-data -R /var/www/git -f
chmod         g+rw -R /var/www/git -f
