.POSIX:

NAME = stagit
VERSION = 0.9.3

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/man
DOCPREFIX = ${PREFIX}/share/doc/${NAME}
SHAREPREFIX = ${PREFIX}/share/${NAME}

LIBCMARK = $(shell pkg-config --silence-errors --libs libcmark-gfm > /dev/null; echo $$?)
CHROMA = $(shell which chroma > /dev/null; echo $$?)
LIBGIT_INC = -I/usr/local/include
LIBS = -L/usr/local/lib `pkg-config --libs libgit2`

ifeq (${CHROMA}, 0)
	STAGIT_CPPFLAGS := ${STAGIT_CPPFLAGS} -DHAS_CHROMA
endif

ifeq (${LIBCMARK}, 0)
	LIBS := ${LIBS} -lcmark-gfm
	STAGIT_CPPFLAGS := ${STAGIT_CPPFLAGS} -DHAS_CMARK
endif

# use system flags.
STAGIT_CFLAGS = ${LIBGIT_INC} ${CFLAGS}
STAGIT_LDFLAGS = ${LIBS} ${LDFLAGS}
STAGIT_CPPFLAGS := ${STAGIT_CPPFLAGS} -D_XOPEN_SOURCE=700 -D_DEFAULT_SOURCE -D_BSD_SOURCE

SRC = \
	src/stagit.c\
	src/stagit-index.c
COMPATSRC = \
	src/reallocarray.c\
	src/strlcat.c\
	src/strlcpy.c\
	src/cp.c
BIN = \
	stagit\
	stagit-index
MAN1 = \
	man/stagit.1\
	man/stagit-index.1
DOC = \
	LICENSE\
	README.md
HDR = src/compat.h

COMPATOBJ = \
	src/reallocarray.o\
	src/strlcat.o\
	src/strlcpy.o\
	src/cp.o

OBJ = ${SRC:.c=.o} ${COMPATOBJ}

all: ${BIN}

.o:
	${CC} -o $@ ${LDFLAGS}

.c.o:
	${CC} -o $@ -c $< ${STAGIT_CFLAGS} ${STAGIT_CPPFLAGS}

dist:
	rm -rf ${NAME}-${VERSION}
	mkdir -p ${NAME}-${VERSION}
	cp -f ${MAN1} ${HDR} ${SRC} ${COMPATSRC} ${DOC} \
		Makefile resources/* contrib/* \
		${NAME}-${VERSION}
	# make tarball
	tar -cf - ${NAME}-${VERSION} | \
		gzip -c > ${NAME}-${VERSION}.tar.gz
	rm -rf ${NAME}-${VERSION}

${OBJ}: ${HDR}

stagit: src/stagit.o ${COMPATOBJ}
	${CC} -o $@ src/stagit.o ${COMPATOBJ} ${STAGIT_LDFLAGS}

stagit-index: src/stagit-index.o ${COMPATOBJ}
	${CC} -o $@ src/stagit-index.o ${COMPATOBJ} ${STAGIT_LDFLAGS}

clean:
	rm -f ${BIN} ${OBJ} ${NAME}-${VERSION}.tar.gz

install: all
	# installing executable files.
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${BIN} ${DESTDIR}${PREFIX}/bin
	for f in ${BIN}; do chmod 755 ${DESTDIR}${PREFIX}/bin/$$f; done
	# installing example files.
	mkdir -p ${DESTDIR}${DOCPREFIX}
	cp -f contrib/*\
		README.md\
		${DESTDIR}${DOCPREFIX}
	mkdir -p ${DESTDIR}${SHAREPREFIX}
	cp -f resources/*\
		${DESTDIR}${SHAREPREFIX}
	# installing manual pages.
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	cp -f ${MAN1} ${DESTDIR}${MANPREFIX}/man1
	install -Dm644 man/* ${DESTDIR}${MANPREFIX}/man1

uninstall:
	# removing executable files.
	for f in ${BIN}; do rm -f ${DESTDIR}${PREFIX}/bin/$$f; done
	# removing example files.
	rm -f ${DESTDIR}${SHAREPREFIX}/*
	rm -f ${DESTDIR}${DOCPREFIX}/*
	rmdir ${DESTDIR}${SHAREPREFIX}
	rmdir ${DESTDIR}${DOCPREFIX}
	# removing manual pages.
	for m in ${MAN1}; do rm -f ${DESTDIR}${MANPREFIX}/man1/$$m; done

.PHONY: all clean dist install uninstall
